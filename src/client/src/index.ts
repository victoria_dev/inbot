import io from "socket.io-client";
import { join } from "path";
import { readConfig, IConfigData } from "../../server/src/config";

const config: IConfigData = readConfig(join(__dirname, "config.json"));
const DEFAULT_JSON = JSON.stringify({ type: "test", query: "node js development" });
const DEFAULT_TEXT = "hello, World!";
const DEFAULT_URL = "localhost:8080/files/1.png";

(function main() {
    let socket = io(`http://${config.serverHost}:${config.serverPort}`);

    socket.on("connect", (io: any) => {
        console.log("I connected to server! HURRAY!!!");
        const tick = setInterval((err) => {
             socket.emit("message", sendMessage(), () => {});
        }, 2000);

        socket.on("error", () => {
            clearInterval(tick);
        });
        socket.on("disconnect", () => {
            console.log("Ошибка соединения");
            clearInterval(tick);
            socket.disconnect();
            main();
        });
    });
})();

function sendMessage() {
    const def_arr = [DEFAULT_JSON, DEFAULT_TEXT, DEFAULT_URL];
    let randIndex = Math.round(Math.random() * 2);
    return def_arr[randIndex];
}






