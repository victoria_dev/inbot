(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./dist/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../node_modules/arguard/index.js":
/*!**********************************************!*\
  !*** D:/inbot/node_modules/arguard/index.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar BOOLEAN = 'boolean';\nvar OBJECT = 'object';\nvar NUMBER = 'number';\nvar STRING = 'string';\nvar FUNCTION = 'function';\nvar UNDEFINED = 'undefined';\nvar MUST_BE = ' must be';\nvar PARAM_NAME = 'paramName';\n\nmodule.exports.bool = _bool;\nfunction _bool(param, paramName) {\n    _string(paramName, PARAM_NAME);\n    if (typeof param !== BOOLEAN) {\n        throw new Error(paramName + MUST_BE + ' a ' + BOOLEAN);\n    }\n}\n\nmodule.exports.func = _func;\nfunction _func (param, paramName) {\n    _string(paramName, PARAM_NAME);\n    if (typeof param !== FUNCTION) {\n        throw new Error(paramName + MUST_BE + ' a ' + FUNCTION);\n    }\n}\n\nmodule.exports.object = _object;\nfunction _object (param, paramName) {\n    _string(paramName, PARAM_NAME);\n    if (typeof param !== OBJECT || !param) {\n        throw new Error(paramName + MUST_BE + ' an ' + OBJECT);\n    }\n}\n\nmodule.exports.array = _array;\nfunction _array (param, paramName) {\n    _string(paramName, PARAM_NAME);\n    if (!Array.isArray(param)) {\n        throw new Error(paramName + MUST_BE + ' an array');\n    }\n}\n\nmodule.exports.number = _number;\nfunction _number (param, paramName) {\n    _string(paramName, PARAM_NAME);\n    if (typeof param !== NUMBER || Number.isNaN(param)) {\n        throw new Error(paramName + MUST_BE + ' a ' + NUMBER);\n    }\n    return {\n        positive: function(){\n            if (param <= 0) {\n                throw new Error(paramName + MUST_BE + ' a positive ' + NUMBER);\n            }\n        }\n    };\n}\n\nmodule.exports.string = _string;\nfunction _string (param, paramName) {\n    if (typeof paramName !== STRING){\n        throw new Error(PARAM_NAME + MUST_BE + ' a ' + STRING);\n    }\n    if (typeof param !== STRING) {\n        throw new Error(paramName + MUST_BE + ' a ' + STRING);\n    }\n    return {\n        oneOf: function(arr){\n            _array(arr, '.oneOf() argument');\n            if (arr.length === 0){\n                throw new Error('empty array passed to .oneOf()');\n            }\n            for (var i = 0; i < arr.length; i++){\n                _string(arr[i], '.oneOf()[' + i + ']');\n                if (arr[i] === param){\n                    return;\n                }\n            }\n            throw new Error(paramName + MUST_BE + ' one of ' + JSON.stringify(arr));\n        },\n        nonempty: function(){\n            if (param === ''){\n                throw new Error(paramName + ' must not be empty');\n            }\n        }\n    };\n}\n\nfunction emptyFunc(){\n    return;\n}\n\n//fequently used arg names\nmodule.exports.names = {\n    params: 'params',\n    options: 'options',\n    cb: 'cb',\n};\n\n//allow undefined\nmodule.exports.maybe = {\n    number: function(param, paramName){\n        _string(paramName, PARAM_NAME);\n        if (typeof param === UNDEFINED){\n            return { positive: emptyFunc };\n        }\n        return _number(param, paramName);\n    },\n    string: function(param, paramName){\n        _string(paramName, PARAM_NAME);\n        if (typeof param === UNDEFINED){\n            return { oneOf: emptyFunc, nonempty: emptyFunc };\n        }\n        return _string(param, paramName);\n    },\n    func: function(param, paramName){\n        _string(paramName, PARAM_NAME);\n        if (typeof param !== UNDEFINED){\n            _func(param, paramName);\n        }\n    },\n    bool: function(param, paramName){\n        _string(paramName, PARAM_NAME);\n        if (typeof param !== UNDEFINED){\n            _bool(param, paramName);\n        }\n    },\n};\n\n//# sourceURL=webpack:///D:/inbot/node_modules/arguard/index.js?");

/***/ }),

/***/ "./dist/config/index.js":
/*!******************************!*\
  !*** ./dist/config/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nvar __importStar = (this && this.__importStar) || function (mod) {\r\n    if (mod && mod.__esModule) return mod;\r\n    var result = {};\r\n    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];\r\n    result[\"default\"] = mod;\r\n    return result;\r\n};\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nconst arguard = __webpack_require__(/*! arguard */ \"../../node_modules/arguard/index.js\");\r\nconst fs = __importStar(__webpack_require__(/*! fs */ \"fs\"));\r\nfunction readConfig(filePath) {\r\n    const data = fs.readFileSync(filePath);\r\n    const conf = JSON.parse(data.toString());\r\n    arguard.object(conf, \"config\");\r\n    arguard.number(conf.serverPort, \"config.serverPort\").positive();\r\n    arguard.string(conf.serverHost, \"config.serverHost\").nonempty();\r\n    arguard.string(conf.logFile, \"config.logFile\").nonempty();\r\n    return conf;\r\n}\r\nexports.readConfig = readConfig;\r\n\n\n//# sourceURL=webpack:///./dist/config/index.js?");

/***/ }),

/***/ "./dist/index.js":
/*!***********************!*\
  !*** ./dist/index.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nvar __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {\r\n    return new (P || (P = Promise))(function (resolve, reject) {\r\n        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }\r\n        function rejected(value) { try { step(generator[\"throw\"](value)); } catch (e) { reject(e); } }\r\n        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }\r\n        step((generator = generator.apply(thisArg, _arguments || [])).next());\r\n    });\r\n};\r\nvar __importStar = (this && this.__importStar) || function (mod) {\r\n    if (mod && mod.__esModule) return mod;\r\n    var result = {};\r\n    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];\r\n    result[\"default\"] = mod;\r\n    return result;\r\n};\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nconst path = __importStar(__webpack_require__(/*! path */ \"path\"));\r\nconst config_1 = __webpack_require__(/*! ./config */ \"./dist/config/index.js\");\r\nconst processor_1 = __webpack_require__(/*! ./processor */ \"./dist/processor/index.js\");\r\nconst logger_1 = __webpack_require__(/*! ./logger */ \"./dist/logger/index.js\");\r\nconst fs = __importStar(__webpack_require__(/*! fs */ \"fs\"));\r\nconst config = config_1.readConfig(path.join(__dirname, \"config.json\"));\r\nconst logger = new logger_1.Logger();\r\nconst http = __webpack_require__(/*! http */ \"http\").createServer().listen(config.serverPort, config.serverHost);\r\nconst processor = new processor_1.Processor(config, http);\r\nprocessor.on(\"message\", (message) => __awaiter(this, void 0, void 0, function* () {\r\n    const msgLog = yield generateMsgLog(message);\r\n    fs.appendFile(config.logFile, msgLog + \"\\n\", (err) => {\r\n        if (err) {\r\n            throw err;\r\n        }\r\n        else {\r\n            logger.log(\"...logged\");\r\n        }\r\n    });\r\n}));\r\nfunction generateMsgLog(msg) {\r\n    const currentTime = new Date();\r\n    return Promise.resolve(`${currentTime}  ${msg}`);\r\n}\r\n\n\n//# sourceURL=webpack:///./dist/index.js?");

/***/ }),

/***/ "./dist/logger/index.js":
/*!******************************!*\
  !*** ./dist/logger/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nclass Logger {\r\n    log(a) {\r\n        console.log(a);\r\n    }\r\n}\r\nexports.Logger = Logger;\r\n\n\n//# sourceURL=webpack:///./dist/logger/index.js?");

/***/ }),

/***/ "./dist/processor/index.js":
/*!*********************************!*\
  !*** ./dist/processor/index.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nvar __importStar = (this && this.__importStar) || function (mod) {\r\n    if (mod && mod.__esModule) return mod;\r\n    var result = {};\r\n    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];\r\n    result[\"default\"] = mod;\r\n    return result;\r\n};\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nconst fs = __importStar(__webpack_require__(/*! fs */ \"fs\"));\r\nconst events_1 = __webpack_require__(/*! events */ \"events\");\r\nconst socket = __importStar(__webpack_require__(/*! socket.io */ \"socket.io\"));\r\nconst isImage = __webpack_require__(/*! is-image-url */ \"is-image-url\");\r\nclass Processor extends events_1.EventEmitter {\r\n    constructor(config, http) {\r\n        super();\r\n        let self = this;\r\n        this.io = socket.listen(http);\r\n        console.log(`Listening ${config.serverHost}:${config.serverPort}...`);\r\n        this.port = config.serverPort;\r\n        this.isFileExists(config.logFile);\r\n        this.io.on(\"connection\", (socket) => {\r\n            console.log(\"user connected\");\r\n            setTimeout(() => {\r\n                console.log(\"(5 seconds)\");\r\n                socket.disconnect();\r\n            }, 5000);\r\n            self.emit(\"message\", `${socket.handshake.address} – New connection`);\r\n            socket.on(\"disconnect\", () => {\r\n                console.log(\"user disconnected\");\r\n                self.emit(\"message\", `${socket.handshake.address} – Disconnected`);\r\n            });\r\n            socket.on(\"error\", (err) => {\r\n                console.log(err);\r\n                self.emit(\"message\", `${socket.handshake.address} –  ${err}`);\r\n            });\r\n            socket.on(\"message\", (data) => {\r\n                const typeMsg = this.getTypeOfMsg(data);\r\n                self.emit(\"message\", `${socket.handshake.address} – New ${typeMsg}: ${data}`);\r\n            });\r\n        });\r\n    }\r\n    getTypeOfMsg(msg) {\r\n        try {\r\n            JSON.parse(msg);\r\n            return \"JSON\";\r\n        }\r\n        catch (err) {\r\n            if (isImage(msg)) {\r\n                return \"IMAGE\";\r\n            }\r\n            else {\r\n                return \"RAW\";\r\n            }\r\n        }\r\n    }\r\n    isFileExists(fullFileName) {\r\n        const isExists = fs.existsSync(fullFileName);\r\n        if (isExists) {\r\n            return;\r\n        }\r\n        else {\r\n            console.log(`File for log creating in ${fullFileName} ...`);\r\n            fs.appendFile(fullFileName, \"\", err => {\r\n                if (err)\r\n                    throw err;\r\n                console.log(\"Done\");\r\n            });\r\n        }\r\n    }\r\n}\r\nexports.Processor = Processor;\r\n\n\n//# sourceURL=webpack:///./dist/processor/index.js?");

/***/ }),

/***/ "events":
/*!*************************!*\
  !*** external "events" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"events\");\n\n//# sourceURL=webpack:///external_%22events%22?");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fs\");\n\n//# sourceURL=webpack:///external_%22fs%22?");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"http\");\n\n//# sourceURL=webpack:///external_%22http%22?");

/***/ }),

/***/ "is-image-url":
/*!*******************************!*\
  !*** external "is-image-url" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"is-image-url\");\n\n//# sourceURL=webpack:///external_%22is-image-url%22?");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),

/***/ "socket.io":
/*!****************************!*\
  !*** external "socket.io" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"socket.io\");\n\n//# sourceURL=webpack:///external_%22socket.io%22?");

/***/ })

/******/ })));