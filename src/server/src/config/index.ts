const arguard = require("arguard");
import * as fs from "fs";

export interface IConfigData {
    logFile: string;
    serverPort: number;
    serverHost: string;
}


export function readConfig(filePath: string): IConfigData {
    const data: Buffer = fs.readFileSync(filePath);
    const conf = JSON.parse(data.toString());
    arguard.object(conf, "config");
    arguard.number(conf.serverPort, "config.serverPort").positive();
    arguard.string(conf.serverHost, "config.serverHost").nonempty();
    arguard.string(conf.logFile, "config.logFile").nonempty();
    return conf;
}
