
import * as path from "path";
import { readConfig, IConfigData } from "./config";
import { Processor } from "./processor";
import { Logger, ILogger } from "./logger";
import * as fs from "fs";


const config: IConfigData = readConfig(path.join(__dirname, "config.json"));
const logger: ILogger = new Logger();

const http = require("http").createServer().listen(config.serverPort, config.serverHost);


const processor: Processor = new Processor(config, http);


processor.on("message", async (message) => {
    const msgLog = await generateMsgLog(message);
    fs.appendFile(config.logFile, msgLog + "\n", (err) => {
        if (err) {
            throw err;
        }
        else {
            logger.log("...logged");
        }
    });
});

function generateMsgLog(msg: string): Promise<string> {
    const currentTime = new Date();
    return Promise.resolve(`${currentTime}  ${msg}`);
}



