
import * as fs from "fs";
import { EventEmitter } from "events";
import { IConfigData } from "../config";
import * as socket from "socket.io";
const isImage = require("is-image-url");



export class Processor extends EventEmitter {
    private port: number;
    private conf: IConfigData;
    private io: any;
    constructor(config: IConfigData, http: any) {
        super();
        let self = this;
        this.io = socket.listen(http);
        console.log(`Listening ${config.serverHost}:${config.serverPort}...`);

        this.port = config.serverPort;
        this.isFileExists(config.logFile);

        this.io.on("connection", (socket: any) => {
            console.log("user connected");
            setTimeout(() => {
                console.log("(5 seconds)");
                socket.disconnect();
            }, 5000);
            self.emit("message", `${socket.handshake.address} – New connection`);

            socket.on("disconnect", () => {
              console.log("user disconnected");
              self.emit("message", `${socket.handshake.address} – Disconnected`);
            });
            socket.on("error", (err: Error) => {
                console.log(err);
                self.emit("message", `${socket.handshake.address} –  ${err}`);
             })
            socket.on("message", (data: string) => {
                const typeMsg = this.getTypeOfMsg(data);
                self.emit("message", `${socket.handshake.address} – New ${typeMsg}: ${data}`);
            });

        });
    }

    getTypeOfMsg(msg: string) {
        try {
            JSON.parse(msg);
            return "JSON";
        }
        catch (err) {
            if (isImage(msg)) {
                return "IMAGE";
            }
            else {
                return "RAW";
            }
        }
    }

    isFileExists(fullFileName: string) {
        const isExists: boolean = fs.existsSync(fullFileName);
        if (isExists) {
            return;
        }
        else {
            console.log(`File for log creating in ${fullFileName} ...`);

            fs.appendFile(fullFileName, "", err => {
                if (err) throw err;
                console.log("Done");
              });
        }
    }
}
