
export interface ILogger {
    log: (a:any)=>void;
}

export class Logger implements ILogger {
    log(a: any) {
        console.log(a);
    }
}