//конфиг для запаковки модуля в 1 файл

var webpack = require('webpack');

module.exports = {
    entry: './server-preprocessed.js',
    output: {
        path: __dirname,
        filename: 'server-bundle.js'
        ,libraryTarget: 'commonjs'
    },
    node: {
        __dirname: false,
        __filename: false,
    }
    ,target: 'node' 
};